<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use App\User;
use App\Permission;
//use Illuminate\Support\Facades\Config;

class Home2Controller extends Controller
{
    public function index()
    {
/*
		$owner = new Role();
		$owner->name         = 'owner';
		$owner->display_name = 'Project Owner'; // optional
		$owner->description  = 'User is the owner of a given project'; // optional
		$owner->save();

		$admin = new Role();
		$admin->name         = 'admin';
		$admin->display_name = 'User Administrator'; // optional
		$admin->description  = 'User is allowed to manage and edit other users'; // optional
		$admin->save();
		
		$user = new User();
		$user->name     = 'admin1';
		$user->email 	= 'admin1@gmail.com';
		$user->password = bcrypt('pwd');
		$user->save();
		$admin = Role::where('name','=','admin')->first();
		$user = User::where('name','=','admin1')->first();

		// role attach alias
		$user->attachRole($admin); // parameter can be an Role object, array, or id
		//equivalent to $user->roles()->attach($admin->id);
		
		$owner = Role::where('name','=','owner')->first();
		$admin = Role::where('name','=','admin')->first();
		
		$createPost = new Permission();
		$createPost->name         = 'create-post';
		$createPost->display_name = 'Create Posts'; // optional
		// Allow a user to...
		$createPost->description  = 'create new blog posts'; // optional
		$createPost->save();

		$editUser = new Permission();
		$editUser->name         = 'edit-user';
		$editUser->display_name = 'Edit Users'; // optional
		// Allow a user to...
		$editUser->description  = 'edit existing users'; // optional
		$editUser->save();

		$admin->attachPermission($createPost);
		// equivalent to $admin->perms()->sync(array($createPost->id));

		$owner->attachPermissions(array($createPost, $editUser));
		// equivalent to $owner->perms()->sync(array($createPost->id, $editUser->id));
		*/
		$user = User::where('name','=','admin1')->first();
		echo '<pre>';
		/*
		print_r(['result'=>$user->hasRole('owner')]);   // false
		print_r(['result'=>$user->hasRole('admin')]);   // true
		print_r(['result'=>$user->can('edit-user')]);   // false
		print_r(['result'=>$user->can('create-post')]); // true
		
		print_r(['result'=>$user->hasRole(['owner', 'admin'])]);       // true
		print_r(['result'=>$user->can(['edit-user', 'create-post'])]); // true

		print_r(['result'=>$user->hasRole(['owner', 'admin'])]);            // true
		print_r(['result'=>$user->hasRole(['owner', 'admin'], true)]);       // false, user does not have admin role
		print_r(['result'=>$user->can(['edit-user', 'create-post'])]);       // true
		print_r(['result'=>$user->can(['edit-user', 'create-post'], true)]); 
		
	
		print_r(['result'=>\Ntrust::hasRole('admin')]);
		print_r(['result'=>\Ntrust::can('create-post')]);
		*/
		
		//print_r(['result'=>$user->ability(array('admin', 'owner'), array('create-post', 'edit-user'))]);
		$options = array(
			'validate_all' => true,
			'return_type' => 'both'
		);

		list($validate, $allValidations) = $user->ability(
			array('admin', 'owner'),
			array('create-post', 'edit-user'),
			$options
		);

		var_dump($validate);
		// bool(false)

		var_dump($allValidations);
		// array(4) {
		//     ['role'] => bool(true)
		//     ['role_2'] => bool(false)
		//     ['create-post'] => bool(true)
		//     ['edit-user'] => bool(false)
		// }

		//$user->ability('admin,owner', 'create-post,edit-user');

		echo '</pre>';
		return view('welcome');
    }

   
}
