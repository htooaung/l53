<?php

namespace App\Http\Controllers;
use App\Http\Requests;
use Illuminate\Http\Request;
use DB;
use PDF;
class PdfController extends Controller
{
    public function index()
    {
        $data = DB::table("statelist")->limit(10)->get();
        return view('pdf.index')->with('data',$data);
    }
 
    public function download(Request $request)
    {
        $data = DB::table("statelist")->limit(10)->get();
        view()->share('data',$data);
        $pdf = PDF::loadView('pdf.pdfview');
        return $pdf->download('pdf.pdfview');
    }
}