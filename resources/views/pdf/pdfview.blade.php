<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Generate in Laravel 5.3</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" >
	</head>
	<body>
		<div class="container">
			<br/>
			<div class="panel panel-info">
			    <div class="panel-heading">Generate PDF in Laravel 5.3</div>
			    <div class="panel-body">
			    	<a href="#"><button type="button" class="btn btn-info btn-sm pull-right">Download PDF</button></a>
			    	<table class="table">
						<tr class="success">
							<th>No</th>
							<th>City Name</th>
							<th>State</th>
						</tr>
						@foreach ($data as $key => $value)
						<tr>
							<td>{{ ++$key }}</td>
							<td>{{ $value->city_name }}</td>
							<td>{{ $value->state }}</td>
						</tr>
						@endforeach
					</table>
			    </div>
			 </div>
			
			
		</div>
	</body>
</html>